#!/bin/sh
cd /home/www-server/redacted/site

# Jade
mkdir -p ./public; cd ./src; ../../tools/jade.js --out ../public rdct.co; cd ..

# Compass
mkdir -p ./public/resources/styles; compass compile

# CoffeeScript
mkdir -p ./public/resources/scripts; coffee -c -o ./public/resources/scripts ./src/resources/scripts
