#!/bin/bash
export NODE_PATH=/usr/local/lib/node_modules

# Clone
cd /home/www-server/
/usr/lib/git-core/git-clone git@github.com:redacted-inc/redacted.git 2>&1 >> /var/log/redacted-deployment-git.log

# Chef
/usr/bin/chef-solo -j /home/www-server/redacted/deployment/chef/node.json -c /home/www-server/redacted/deployment/chef/solo.rb 2>&1 >> /var/log/redacted-chef.log

# Compile
cd /home/www-server/redacted/site

# Jade
mkdir -p ./public; cd ./src; ../../tools/jade.js --out ../public rdct.co 2>&1 >> /var/log/redacted-deployment-jade.log

# Compass
mkdir -p ./public/resources/styles; compass compile 2>&1 >> /var/log/redacted-deployment-compass.log

mkdir /home/www-server/init
touch /home/www-server/init/cloned
chown -R www-server:www /home/www-server/
chmod -R ug+rw /home/www-server
