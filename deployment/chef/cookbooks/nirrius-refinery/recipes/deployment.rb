#
# Cookbook Name:: nirrius-cms
# Recipe:: default
#
# Copyright 2011, Redacted Inc.
#
# All rights reserved - Do Not Redistribute
#

script "install-deps" do
  interpreter "bash"
  user "root"
  cwd "/home/www-server/redacted/site"
  code <<-EOH
    git clone git@bitbucket.org:redacted_inc/nirrius-refinery.git
    cd nirrius-refinery
    bundle install
  EOH
end

# Add service to start the server
#template "/etc/init/rails-nirrius.conf" do
#  source "rails-nirrius.conf.erb"
#end

#service "rails-nirrius" do
#  provider Chef::Provider::Service::Upstart
#  action [ :start, :enable ]
#end 
