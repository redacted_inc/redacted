#
# Cookbook Name:: nirrius-cms
# Recipe:: default
#
# Copyright 2011, YOUR_COMPANY_NAME
#
# All rights reserved - Do Not Redistribute
#

bash "setup-refinerycms" do
  user "root"
  cwd "/home/www-server/redacted/site"
  code <<-EOH
    refinerycms nirrius-refinery
  EOH
end

template "/home/www-server/redacted/site/nirrius-refinery/Gemfile" do
  source "nirrius-Gemfile.erb"
end

template "/home/www-server/redacted/site/nirrius-refinery/config/environment.rb" do
  source "environment.rb.erb"
end

script "add-engine-databases" do
  interpreter "bash"
  user "root"
  cwd "/home/www-server/redacted/site/nirrius-refinery"
  code <<-EOH
    rails g refinerycms_blog
    rake db:migrate
  EOH
end

script "install-deps" do
  interpreter "bash"
  user "root"
  cwd "/home/www-server/redacted/site/nirrius-refinery"
  code <<-EOH
    bundle install
  EOH
end
