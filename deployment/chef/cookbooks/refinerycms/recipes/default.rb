#
# Cookbook Name:: refinerycms
# Recipe:: default
#
# Copyright 2011, Redacted Inc
#
# All rights reserved - Do Not Redistribute
#

package "sqlite3"
package "libsqlite3-ruby"
package "libsqlite3-ruby1.8"
package "libsqlite3-ruby1.9.1"
package "libsqlite3-dev"

gem_package "refinerycms" do
  options "--no-ri --no-rdoc"
end

# Used for S3 Deployment
package "libxslt-dev"
package "libxml2-dev"
gem_package "nokogiri" do
  options "--no-ri --no-rdoc"
end

gem_package "fog" do
  options "--no-ri --no-rdoc"
end
