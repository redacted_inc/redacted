# redacted inc.
This repo contains all code related to the rdct.co and nirri.us system.

## License
Redacted company and nirrius gaming community website, by Redacted Inc. is licensed under a Creative Commons Attribution-ShareAlike 3.0 Unported License.
Based on a work at http://github.com .
Permissions beyond the scope of this license may be available at http://rdct.co/about/ .
